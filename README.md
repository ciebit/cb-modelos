# cb Modelos #

Módulo para classes genéricas que serem de suporte para os demais módulos CB.

## Instalação ##

### Composer ###

No arquivo "composer.json" realize as seguintes adições:

* Adicionar no item "require" a dependências "ciebit/cb-modelos";
* Adicionar no item "repositories" o repositório "git@bitbucket.org:ciebit/cb-modelos"
