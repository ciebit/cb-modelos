<?php

require __DIR__.'/../vendor/autoload.php';

class Pessoas extends \Ciebit\Modelos\Lista
{
    public function adicionar($nome):self
    {
        $this->lista[] = $nome;
        return $this;
    }
}

$ListaPessoas = new Pessoas;

$ListaPessoas
->adicionar('Maria')
->adicionar('Pedro')
->adicionar('Marcelo')
->adicionar('Joana')
->adicionar('Ana')
;

echo "Testando obtencao\n";
var_dump($ListaPessoas->obter(0));
var_dump($ListaPessoas->obter(3));
var_dump($ListaPessoas->obter(5));
echo PHP_EOL;

echo "Testando obtencao de lista\n";
var_dump($ListaPessoas->obterLista());
echo PHP_EOL;

echo "Testando obtencao em sequencia\n";
var_dump($ListaPessoas->obterProximo());
var_dump($ListaPessoas->obterProximo());
var_dump($ListaPessoas->obterProximo());
echo PHP_EOL;

echo "Zerando Indice\n";
$ListaPessoas->zerarIndice();
echo PHP_EOL;

echo "Obtendo o próximo\n";
var_dump($ListaPessoas->obterProximo());
echo PHP_EOL;

echo "Testando obtencao de total\n";
var_dump($ListaPessoas->total());
