<?php

namespace Ciebit\Modelos;

abstract class Lista
{
    protected $lista = array();

    /**
     * Mistura os valores do array
     */
    public function embarralhar():self
    {
        shuffle($this->lista);
        return $this;
    }

    /**
     * Retorna um elemento do array conforme a
     * possição passada
     */
    public function obter(int $pos)
    {
        if (! isset( $this->lista[$pos])) {
            return false;
        }
        return $this->lista[$pos];
    }

    /**
     * Retorna a lista de objetos
     */
    public function obterLista():array
    {
        return $this->lista;
    }

    /**
     * Retorna o elemento atualmente apontado
     */
    public function obterProximo()
    {
        $atual = current($this->lista);

        if (! $atual ) {
            reset($this->lista);
        } else {
            next($this->lista);
        }

        return $atual;
    }

    /**
     * Retorna o total de itens
     */
    public function total():int
    {
        return count($this->lista);
    }

    /**
     * Volta o ponteiro da lista para o primeiro item
     */
    public function zerarIndice():self
    {
        reset($this->lista);
        return $this;
    }
}
